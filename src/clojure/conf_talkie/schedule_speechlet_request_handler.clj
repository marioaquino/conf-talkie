(ns conf-talkie.schedule-speechlet-request-handler
  (:gen-class
    :name conf-talkie.ScheduleSpeechletRequestHandler
    :extends com.amazon.speech.speechlet.lambda.SpeechletRequestStreamHandler
    :init "init"
    :constructors {[] [com.amazon.speech.speechlet.Speechlet java.util.Set]})
  
  (:use [conf-talkie.intent.schedule-intent])
  
  (:import [com.climate.boomhauer BoomhauerSpeechlet]))

(defn -init []
  [[(BoomhauerSpeechlet.), #{}] nil])

