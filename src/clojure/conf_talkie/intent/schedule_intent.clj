(ns conf-talkie.intent.schedule-intent
  (:require [clojure.string :as str]
            [clojure.tools.logging :as log]
            [com.climate.boomhauer.intent-handler :refer [defintent]]
            [conf-talkie.client.google-sheet :as sheet])
  (:import [com.amazon.speech.speechlet SpeechletResponse]
           [com.amazon.speech.ui PlainTextOutputSpeech Reprompt]))

(def me "Mario Aquino")

(defn- mk-plain-speech [text]
  (doto (PlainTextOutputSpeech.) (.setText text)))

(defn- mk-speech-and-reprompt
  ([text] (mk-speech-and-reprompt text text))
  ([speech-text reprompt-text]
   (let [speech (mk-plain-speech speech-text)
         reprompt-speech (mk-plain-speech reprompt-text)
         reprompt (doto (Reprompt.) (.setOutputSpeech reprompt-speech))]
     [speech reprompt])))

(defn- mk-tell-response [text]
  (SpeechletResponse/newTellResponse (mk-plain-speech text)))

(defn- mk-ask-response
  ([text] (mk-ask-response text text))
  ([speech-text reprompt-text]
   (let [[speech reprompt] (mk-speech-and-reprompt speech-text reprompt-text)]
     (SpeechletResponse/newAskResponse speech reprompt))))

(defn browse-talks [session session-map]
  (mk-tell-response "Lets browse some talks, Mario Enrique."))

(defn- get-cached-interaction-data [session]
  (.getAttribute session "last-interaction"))

(defn- cache-schedule-data [data session]
  (.setAttribute session "last-interaction" data)
  data)

(defn- find-talk-slot-details
  ([speaker talks-by-day]
   (find-talk-slot-details speaker talks-by-day (fn [time talk-slots] time)))
  ([speaker talks-by-day result-fn]
   (reduce (fn [storage [day talks]]
             (let [get-details-for-speaker (fn [talks-for-day speaker]
                                          (some (fn [[time & talk-slots]]
                                                  (if (some #(= speaker (get % "speaker")) talk-slots)
                                                    (result-fn time talk-slots)))
                                                talks-for-day))]
               (if-some [details (get-details-for-speaker talks speaker)]
                 (conj storage day details)
                 storage)))
           []
           talks-by-day)))

(defn get-my-time [session session-map]
  (let [talks-by-day (:conf-schedule (sheet/cache-clojure-west-schedule session session-map))
        [day time] (find-talk-slot-details me talks-by-day)
        speech-text (format "Your talk is on %s from %s" day time)]
    (mk-ask-response speech-text) ))

(defn get-my-competition [session session-map]
  (let [talks-by-day (:conf-schedule (sheet/cache-clojure-west-schedule session session-map))
        competition (fn [time talk-slots] (remove #(= me (get % "speaker")) talk-slots))
        [_ other-slots] (find-talk-slot-details me talks-by-day competition)
        _ (cache-schedule-data other-slots session)
        serialize-speaker-list (fn [speakers-comma-separated]
                                 (str/replace speakers-comma-separated "," " and "))
        build-talk-details (fn [talk-slot]
                             (let [talk (get talk-slot "talk")
                                   speaker-list (get talk-slot "speaker")
                                   speakable-speakers (serialize-speaker-list speaker-list)]
                               (str talk " by " speakable-speakers)))
        other-slots->speech-text (-> (map build-talk-details other-slots))        
        speech-text (apply format "The other talks scheduled at the same time as yours are %s and %s" other-slots->speech-text)]
    (mk-ask-response speech-text)))

(defn- get-abstract-by-title [session session-map talk-title]
  (let [talk-details (:talk-details (sheet/cache-clojure-west-talk-details session session-map))
        talk (first (filter #(= talk-title (get % "talkTitle")) talk-details))]
    (get talk "abstract")))

(def stupid-number-conversion 
  (zipmap ["1st" "second" "third" "fourth" "fifth"] (range)))

(def abstract-reprompt-text "I'm not sure which abstract you wanted me to read. Can you ask again?")

(defn read-relative-abstract [session session-map]
  (if-let [talks (get-cached-interaction-data session)]
    (let [order-phrase (:order session-map)
          order (get stupid-number-conversion order-phrase)]
      (if order
        (let [talk-title (-> talks (nth order) (get "talk"))
              abstract (get-abstract-by-title session session-map talk-title)]
          (mk-tell-response abstract))))
    (mk-ask-response abstract-reprompt-text)))

(defintent :ScheduleIntent browse-talks)
(defintent :MariosScheduleIntent get-my-time)
(defintent :MariosTalkCompetitionIntent get-my-competition)
(defintent :ReadRelativeAbstract read-relative-abstract)
