(ns conf-talkie.client.google-sheet
  (:require [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.tools.logging :as log]
            [google-apps-clj.google-sheets :as sheets]
            [clojure.tools.logging :as log]))

(def my-creds (edn/read-string (slurp (io/resource "conf_talkie/google-creds.edn"))))

(defn- get-sheet-service [creds]
  (sheets/build-sheet-service creds))

(defn- get-spreadsheet-by-title [title service]
  (:spreadsheet (sheets/find-spreadsheet-by-title service title)))

(defn- get-worksheet-by-title [spreadsheet worksheet-title service]
  (:worksheet (sheets/find-worksheet-by-title service spreadsheet worksheet-title)))

(defn- get-values-from-worksheet [worksheet service]
  (sheets/read-worksheet-values service worksheet))

(defn- get-sheet-data [worksheet-title]
  (let [spreadsheet-title "ClojureWestSchedule"
        service (get-sheet-service my-creds)]
    (-> (get-spreadsheet-by-title spreadsheet-title service)
        (get-worksheet-by-title worksheet-title service)
        (get-values-from-worksheet service))))

(defn- transform-schedule [schedule]
  (map (fn [[time & talks]]
         (conj (map
                (fn [talk]
                  (zipmap ["talk" "speaker"] (str/split-lines talk)))
                talks)
               time))
       schedule))

(defn- get-clojure-west-schedule []
  (let [days ["Friday" "Saturday"]
        talks-for-days #(get-sheet-data %)
        talks-processor (comp transform-schedule talks-for-days)
        talks-by-day (zipmap days (map talks-processor days))]
    talks-by-day))

(defn- get-clojure-west-talk-details []
  (let [talk-data (get-sheet-data "talk-details")
        columns ["name" "last" "twitter" "personalURL" "company" "companyURL"
                 "speakerBio" "secondSpeakerInfo" "talkTitle" "abstract" "category"]
        key-talk-data (fn [row] (zipmap columns row))]
    (map key-talk-data talk-data)))

(defn- cache-in-session-map [session session-map data cache-key]
  (.setAttribute session (str cache-key) data)
  (assoc session-map (keyword cache-key) data))

(defn cache-clojure-west-schedule [session session-map]
  (if (:conf-schedule session-map)
    session-map
    (cache-in-session-map session session-map (get-clojure-west-schedule) :conf-schedule)))

(defn cache-clojure-west-talk-details [session session-map]
  (if (:talk-details session-map)
    session-map
    (cache-in-session-map session session-map (get-clojure-west-talk-details) :talk-details)))
