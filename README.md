# Alexa Voice Services Sample App #

This repo contains a simple, sample implementation of an AWS Lambda
function that handles interactions from Alexa Voice Services (AVS). This
code was originally prepared for demonstration at the Clojure/west
2016 conference by Mario Aquino for the talk: "The Age of Talkies".

This sample app uses the Boomhauer library for simplifying AVS intent
registration and response.
