(defproject conf-talkie "0.1.0"
  :description
  "FIXME: write description"
  :url
  "https://bitbucket.org/marioaquino/conf-talkie"
  :min-lein-version "2.0.0"

  :source-paths ["src/clojure"]
  :test-paths ["test/clojure" "test/resources"]
  :resource-paths ["src/resources"]

  :dependencies
  [[org.clojure/data.json "0.2.6"]
   [com.amazonaws/aws-lambda-java-core "1.1.0"]
   [com.amazonaws/aws-lambda-java-events "1.1.0"]
   [com.amazon.alexa/alexa-skills-kit "1.1.3"]
   [com.climate/boomhauer "0.1.0"]
   [org.clojure/clojure "1.8.0"]
   [clj-http "2.1.0"]
   [cheshire "5.5.0"]
   [clj-time "0.11.0"]
   [google-apps-clj "0.5.1"]
   [org.clojure/tools.logging "0.3.1"]
   [org.slf4j/slf4j-api "1.7.19"]
   [org.slf4j/slf4j-log4j12 "1.7.19"]
   [log4j/log4j "1.2.17"]
   [org.apache.commons/commons-lang3 "3.4"]
   [commons-io/commons-io "2.4"]]

  :aot [conf-talkie.schedule-speechlet-request-handler]

  :test-selectors {:default (complement :integration)
                   :integration :integration
                   :all (constantly true)})
